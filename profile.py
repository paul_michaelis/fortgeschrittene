from tree import Tree
import random
import aho
import networkx as nx


class Profile:
    """
    mainly makes sure that the trees have nodes consistently labelled with the trees they belong to
    :param trees which comprise the profile
    """
    def __init__(self, trees):
        self.trees = []

        for tree in trees:
            nodes = tree.nodes()
            dic = {}
            # Each node is assigned its tree
            for node in nodes:
                copy = node
                copy.tree = tree
                dic[node] = copy
            new_tree = nx.relabel_nodes(tree, dic)

            self.trees.append(new_tree)

        self.count = len(trees)

    @staticmethod
    def make_subtrees(initial_tree, count):
        """
        Generates subtrees from a tree. Can be used when testing BUILDST
        :param initial_tree: The initial tree
        :param count: Number of subtrees to be generated
        :return: List of trees
        """
        triples = initial_tree.get_triples_recursively()
        random.shuffle(triples)
        tree_list = []
        for i in range(count):
            start = int(i * len(triples) / count)
            end = int((i + 1) * len(triples) / count)
            subtriples = triples[start: end]
            subtree = aho.aho(subtriples, Tree.get_leaveset_from_triples(subtriples))

            for node in subtree:
                node.tree = i

            tree_list.append(subtree)
        return tree_list

    def position_array(self):
        array = []
        for x in self.trees:
            array.append(x[1])
        return array
