import networkx as nx
import itertools
from knoten import Knoten
from triple import Triple
import time
import copy


class CBMG(nx.DiGraph):
    def __init__(self, tr):
        """
        :param tr: A CBMG is initialised with a coloured tree of class Tree.
        """
        super().__init__()
        leaf_nodes = tr.get_leaveset_from_tree()
        leaf_dict = {}

        for leaf in leaf_nodes:
            if leaf.colour is None:
                raise ValueError('Tree is not coloured (see tree.Tree.paint_tree())')

        for node in leaf_nodes:
            leaf_dict.setdefault(node.colour, []).append(node)

        cols = set(leaf_dict.keys())
        self.colours = cols

        for node in leaf_nodes:
            for colour in cols.difference([node.colour]):
                best_matches = tr.find_best_match(node, leaf_dict[colour])
                for match in best_matches:
                    self.add_edge(node, match)

        self.mark_thinness()

    def N_plus(self, node):
        """
        :param node: A node in the CBMG or a set of nodes
        :return: All outneighbours of the node(s)
        """
        if type(node) == Knoten:
            return set(self.successors(node))
        elif type(node) == set:
            out = []
            for n in node:
                out.extend(list(self.N_plus(n)))
            return set(out)
        else:
            raise TypeError('node has to be either of class Knoten or set')

    def N_minus(self, node):
        """
        :param node: A node in the CBMG or a set of nodes
        :return: All inneighbours of the node(s)
        """
        if type(node) == Knoten:
            return set(self.predecessors(node))
        elif type(node) == set:
            out = []
            for n in node:
                out.extend(list(self.N_plus(n)))
            return set(out)
        else:
            raise TypeError('node has to be either of class Knoten or set')

    def all_neighbours(self, node):
        """
        :param node: A node in the CBMG
        :return: All in- and outneighbours of the node
        """
        neighbours = []
        neighbours.extend(self.N_plus(node))
        neighbours.extend(self.N_minus(node))
        return neighbours

    def mark_thinness(self):
        """
        Marks each node with its thinness class in place
        """
        for node in self.nodes():
            node.out_neighbours = self.N_plus(node)
            node.in_neighbours = self.N_minus(node)

        thin_class = 0
        for node in self.nodes():
            if not node.thinness:
                thin_class += 1
                node.thinness = thin_class
            for other_node in self.nodes():
                if not other_node.thinness:
                    if node.in_neighbours == other_node.in_neighbours and node.out_neighbours == other_node.out_neighbours:
                        other_node.thinness = thin_class

    def get_thinness_dic(self):
        """
        :return: A thinness dictionary from the CBMG. Thinness classes are keys and respective nodes are values.
        """
        classes = {}
        for node in self.nodes():
            if node.thinness not in classes:
                classes[node.thinness] = [node]
            else:
                classes[node.thinness].append(node)
        return classes

    def get_2cbmgs(self):
        """
        :return: A list of all possible 2-cbmgs
        """
        two_cbmgs = []

        # If there are more than two colours all possible 2-cbmgs from the combinations of two colours
        if len(self.colours) > 2:
            for combination in itertools.combinations(self.colours, 2):

                bmg_copy = self.copy()
                #bmg_copy = copy.deepcopy(self)

                for node in self.nodes():
                    if node.colour not in combination:
                        bmg_copy.remove_node(node)

                # When calling the copy() function on self, a Tree object is created. Here the Tree object is converted
                # to a CBMG object
                bmg_copy.__class__ = CBMG
                two_cbmgs.append(bmg_copy)
            for i in two_cbmgs:
                i.mark_thinness()
            return two_cbmgs
        # If there are only two colours, self is already a 2-cbmg and it is returned as the only element in a list
        elif len(self.colours) == 2:
            self.mark_thinness()
            return [self]
        # If there is only one colour in the cbmg an exception is thrown
        else:
            raise Exception('Error: BMG contains only one colour.')

    def check_2cbmg(self):
        """
        Checks a 2-CBMG for the three axioms N1, N2, N3
        :return: True, if all axioms pass, False otherwise
        """
        thinness_dic = self.get_thinness_dic()

        for combination in itertools.permutations(thinness_dic.keys(), 2):
            # Alpha contains the nodes that have the thinness class alpha, same for beta
            alpha = thinness_dic[combination[0]]
            beta = thinness_dic[combination[1]]
            if not (self.check_N1(alpha, beta) and self.check_N2(alpha) and self.check_N3(alpha, beta)):
                return False
        return True

    def check_N1(self, alpha, beta):
        if not set.intersection(set(alpha), self.N_plus(beta[0])) \
                and not set.intersection(set(beta), self.N_plus(alpha[0])):

            if not set.intersection(set(self.N_plus(alpha[0])), self.N_plus(self.N_plus(beta[0]))) \
                    and not set.intersection(set(self.N_plus(beta[0])), self.N_plus(self.N_plus(alpha[0]))):
                return True
            return False
        return True

    def check_N2(self, alpha):
        if set.issubset(set(self.N_plus(self.N_plus(self.N_plus(alpha[0])))), self.N_plus(alpha[0])):
            return True
        return False

    def check_N3(self, alpha, beta):
        if not set.intersection(set(alpha), self.N_plus(self.N_plus(beta[0]))) \
                and not set.intersection(set(beta), self.N_plus(self.N_plus(alpha[0]))) \
                and set.intersection(self.N_plus(alpha[0]), self.N_plus(beta[0])):
            # implies:
            if self.N_minus(alpha[0]) == self.N_minus(beta[0]) \
                    and (set.issubset(self.N_plus(alpha[0]), self.N_plus(beta[0]))
                         or set.issubset(self.N_plus(beta[0]), self.N_plus(alpha[0]))):
                return True
            return False
        return True

    def get_informative_triples(self):
        """
        For each permutation of three nodes in the CBMG the four possible informative triple situations are checked.
        :return: List of triples
        """
        triples = []
        # If there are less than 3 nodes in the cmbg, no triples can be formed
        if len(self.nodes()) < 3:
            return triples

        for permutation in itertools.permutations(self.nodes(), 3):
            a = permutation[0]
            b = permutation[1]
            c = permutation[2]

            if a.colour != b.colour:

                # 1: a and b are reciprocal best matches, c is not connected to a or b
                if a in self.N_plus(b) and b in self.N_plus(a) \
                        and c not in self.all_neighbours(a) and c not in self.all_neighbours(b):

                    # bitscore and evalue of the egde from a to b are integrated into the triple
                    bitscore, evalue = self.get_tripel_attr(a, b)

                    triples.append(Triple(a, b, c, bitscore=bitscore, evalue=evalue))

                if c.colour != a.colour:
                    # 2: a and b are reciprocal best matches, a is the best match of c
                    if a in self.N_plus(b) and b in self.N_plus(a) \
                            and a in self.N_plus(c) and c not in self.N_plus(a) and c not in self.all_neighbours(b):

                        # bitscore and evalue of the egde from a to b are integrated into the triple
                        bitscore, evalue = self.get_tripel_attr(a, b)

                        triples.append(Triple(a, b, c, bitscore=bitscore, evalue=evalue))

                    # 3: b is the best match of a, no other matches
                    if b in self.N_plus(a) and a not in self.N_plus(b) and c not in self.all_neighbours(a) \
                            and c not in self.all_neighbours(b):

                        # bitscore and evalue of the egde from a to b are integrated into the triple
                        bitscore, evalue = self.get_tripel_attr(a, b)

                        triples.append(Triple(a, b, c, bitscore=bitscore, evalue=evalue))

                    # 4: b is the best match of a, a is the best match of c, no other matches
                    if a in self.N_plus(c) and c not in self.N_plus(a) and b in self.N_plus(a) \
                            and a not in self.N_plus(b) and c not in self.all_neighbours(b):

                        # bitscore and evalue of the egde from a to b are integrated into the triple
                        bitscore, evalue = self.get_tripel_attr(a, b)

                        triples.append(Triple(a, b, c, bitscore=bitscore, evalue=evalue))

        return triples

    def get_tripel_attr(self, a, b):
        """
        :param a: Node in CBMG
        :param b: Node in CBMG
        :return: bitscore and evalue of the edge from a to b
        """
        bitscore = 1
        evalue = 1
        attr_dic = self.get_edge_data(a, b)
        try:
            bitscore = attr_dic['bitscore']
            evalue = attr_dic['evalue']
        except KeyError:
            pass
        return bitscore, evalue

    # Get hierarchy (R') for cbmg
    def hierarchy(self):
        """
        :return: Hierarchy R' for a CBMG (as a list of lists)
        """
        hierarchy = []
        dic = self.get_thinness_dic()
        
        # Für jede Dünnheitsklasse a wird R(a) und Q(a) berechnet, diese sets werden zusammengefügt und in die
        # Hierarchie eingefügt
        for thinness_class in dic:
            node = dic[thinness_class][0]
            r = set(nx.descendants(self, node))
            q = set()
            for other_thinness_class in dic:
                other_node = dic[other_thinness_class][0]
                if self.N_minus(node) == self.N_minus(other_node) and set(
                        self.N_plus(dic[other_thinness_class][0])).issubset(set(self.N_plus(dic[thinness_class][0]))):
                    q.update(dic[other_thinness_class])
            rq = list(q.union(r))
            rqsorted = CBMG.sortnodes(rq)
            hierarchy.append(rqsorted)

        # The sets of individual leaves are added
        for node in self.nodes():
            hierarchy.append([node])
        # Sort hierarchy, sort() does not work on nodes
        hierarchy = CBMG.sortnodes(hierarchy)
        # Remove consecutive duplicates
        hierarchy = [hierarchy[i] for i in range(len(hierarchy)) if i == 0 or hierarchy[i] != hierarchy[i-1]]
        # Add the rootnode as the last (the complete set)
        rootnode = CBMG.sortnodes(list(self.nodes()))
        hierarchy.append(rootnode)

        return hierarchy

    @staticmethod
    def sortnodes(nodelist):
        outlist = []
        while nodelist:
            outnode = nodelist[0]
            for node in nodelist:
                if node < outnode:
                    outnode = node
            outlist.append(outnode)
            nodelist.remove(outnode)
        return outlist

