class Triple:
    def __init__(self, x, y, z, bitscore=1, evalue=1, simscore=1):
        """
        :param x: First member of in group
        :param y: Second member of ingroup
        :param z: Outgroup
        :param bitscore: bitscore is 1 by default (very bad)
        :param evalue: evalue is 1 by default (very bad)
        :param simscore: simscore is 1 by default (very bad)
        """
        self.x = x
        self.y = y
        self.z = z
        self.bitscore = bitscore
        self.evalue = evalue
        self.simscore = simscore

    def __str__(self):
        # x,y|z
        return str(self.x) + ',' + str(self.y) + '|' + str(self.z)

    def __eq__(self, other):
        # If the set of the ingroups, and the outgroups are identical the triples are identical
        if self.get_ingroup() == other.get_ingroup() and self.z == other.z:
            return True
        return False

    def get_ingroup(self):
        return {self.x, self.y}

    def __hash__(self):
        return hash((self.x, self.y, self.z))
