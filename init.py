from position import Position
from tree import Tree
from cbmg import CBMG
from knoten import Knoten
from triple import Triple
import aho
import time
import networkx as nx
from profile import Profile
import deng
import matplotlib.pyplot as plt
import matplotlib as mp
import parser


def test(knotenzahl, farbenzahl):
    print('%d Knoten, %d Farben:' % (knotenzahl, farbenzahl))

    colours = [str(i) for i in range(farbenzahl)]
    tripel_times = []
    aho_times = []
    hasse_times = []
    deng_times = []

    for i in range(1):
        rt = Tree.random_tree(knotenzahl, 30, 3)

        rt.paint_tree(colours)
        cbmg = CBMG(rt)

        cbmg2 = cbmg.get_2cbmgs()

        one = time.time()
        all_informative_tripels = []
        for i in cbmg2:
            triples = i.get_informative_triples()
            all_informative_tripels.extend(triples)
        two = time.time()
        tripel_times.append(two - one)

        one = time.time()
        ahobaum = aho.aho(all_informative_tripels)
        two = time.time()
        aho_times.append(two - one)

        one = time.time()
        hasse_trees = []
        for i in cbmg2:
            hierarchy = i.hierarchy()
            h_tree = Tree.construct_hasse(hierarchy)
            hasse_trees.append(h_tree)
        two = time.time()
        hasse_times.append(two - one)

        one = time.time()
        prof = Profile(hasse_trees)
        pos = Position([tree.get_root() for tree in prof.trees])
        dengbaum = deng.BUILDST(pos)
        two = time.time()
        deng_times.append(two - one)

    print('    Generate triples (avg of three): ' + str(sum(tripel_times) / len(tripel_times)))
    print('    Generate Aho tree (avg of three): ' + str(sum(aho_times) / len(aho_times)))
    print('    Generate Hasse trees (avg of three): ' + str(sum(hasse_times) / len(hasse_times)))
    print('    Generate deng tree (avg of three): ' + str(sum(deng_times) / len(deng_times)))
    print()


def test_incons_tr(knotenzahl, percentage):
    print('%d Knoten, %s Prozent inkonsistente Tripel (5 Farben):' % (knotenzahl, str(percentage)))

    tripel_times = []
    aho_times = []
    isomorphic = []

    farbenzahl = 5
    colours = [str(i) for i in range(farbenzahl)]

    for i in range(5):
        rt = Tree.random_tree(knotenzahl, 30, 3)

        rt.paint_tree(colours)
        cbmg = CBMG(rt)

        cbmg2 = cbmg.get_2cbmgs()

        one = time.time()
        all_informative_tripels = []
        for i in cbmg2:
            triples = i.get_informative_triples()
            all_informative_tripels.extend(triples)
        two = time.time()
        tripel_times.append(two - one)

        new_triples = Tree.make_triples_inconsistent(all_informative_tripels, percentage)

        ahobaum_no_incons = aho.aho(all_informative_tripels)
        one = time.time()
        ahobaum = aho.aho(new_triples)
        two = time.time()
        aho_times.append(two - one)

        isomorphic.append(nx.is_isomorphic(ahobaum_no_incons, ahobaum))

    print('    Generate triples (avg of five): ' + str(sum(tripel_times) / len(tripel_times)))
    print('    Generate Aho tree (avg of five): ' + str(sum(aho_times) / len(aho_times)))
    print('    Is the tree made from all inf. triples isomorphic to the random tree?: ' + str(
        nx.is_isomorphic(rt, ahobaum_no_incons)))
    print('    Are the five trees isomorphic to the Aho tree with no inconsistent triples?: ' + str(isomorphic))
    print()


def test_deleted_tr(knotenzahl, percentage):
    print('%d Knoten, %s Prozent gelöschte Tripel (5 Farben):' % (knotenzahl, str(percentage)))

    tripel_times = []
    aho_times = []
    isomorphic = []

    farbenzahl = 5
    colours = [str(i) for i in range(farbenzahl)]

    for i in range(5):
        rt = Tree.random_tree(knotenzahl, 30, 3)

        rt.paint_tree(colours)
        cbmg = CBMG(rt)

        cbmg2 = cbmg.get_2cbmgs()

        one = time.time()
        all_informative_tripels = []
        for i in cbmg2:
            triples = i.get_informative_triples()
            all_informative_tripels.extend(triples)
        two = time.time()
        tripel_times.append(two - one)

        new_triples = Tree.remove_triple(all_informative_tripels, percentage)

        ahobaum_no_deletions = aho.aho(all_informative_tripels)
        one = time.time()
        ahobaum = aho.aho(new_triples)
        two = time.time()
        aho_times.append(two - one)

        isomorphic.append(nx.is_isomorphic(ahobaum_no_deletions, ahobaum))

    print('    Generate triples (avg of five): ' + str(sum(tripel_times) / len(tripel_times)))
    print('    Generate Aho tree (avg of five): ' + str(sum(aho_times) / len(aho_times)))
    print('    Is the tree made from all inf. triples isomorphic to the random tree?: ' + str(
        nx.is_isomorphic(rt, ahobaum_no_deletions)))
    print('    Are the five trees isomorphic to the Aho tree with no deleted triples?: ' + str(isomorphic))
    print()


# test_incons_tr(50, 0)
# test_incons_tr(50, 0.001)
# test_incons_tr(50, 0.01)
# test_incons_tr(50, 0.1)
# test_incons_tr(50, 1)
# test_incons_tr(50, 10)
#
# test_deleted_tr(50, 0)
# test_deleted_tr(50, 0.001)
# test_deleted_tr(50, 0.01)
# test_deleted_tr(50, 0.1)
# test_deleted_tr(50, 1)
# test_deleted_tr(50, 10)

# test(500, 3)
# test(500, 5)
# test(500, 10)
# test(500, 20)
#
# test(1000, 3)
# test(1000, 5)
# test(1000, 10)
test(1000, 20)
#
# test(200, 3)
# test(200, 5)
# test(200, 10)
# test(200, 20)


# '''' Time testing for deng vs aho'''
# # one = time.time()
# # trees = Tree.random_tree(200, 40, 2)
# # print(trees)
# # print(len(trees.nodes()))
# # two = time.time()
# # print('Random tree: ' + str(two - one))
# # print()
# #
# # tripels = Tree.get_tripels_recursively(trees)
# #
# # prof = Profile(Profile.make_subtrees(trees, 1))
# #
# # pos = Position([ tree.get_root() for tree in prof.trees ])
# #
# # start = time.time()
# #
# # buildst = deng.BUILDST(pos)
# # print(buildst)
# #
# # deng = time.time()
# # print('Deng BUILDST: ' + str(deng - start))
# # print()
# #
# # start_2 = time.time()
# # ahobaum = aho.aho(tripels)
# # print(ahobaum)
# #
# # aho = time.time()
# # print('Aho BUILD: ' + str(aho - start_2))
