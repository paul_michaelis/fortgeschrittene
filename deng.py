from tree import Tree
from knoten import Knoten
import networkx as nx
from position import Position


def BUILDST(position, tree=None, root=None):
    """
    BUILDST Algorithm (Deng et al.)
    :param position: Initially the roots of the subtrees. The profile is implied because each node in the position
    is associated with its tree
    :param tree: Used internally to build the tree
    :param root: Used internally to build the tree
    :return: A tree unifying the subtrees of the profile
    """
    if tree is None:
        tree = Tree()

    # 1: Create new root
    new_root = Knoten(str(len(tree.nodes())))

    # 2-4: If connected component is only one node
    subnode = position.subnodes()
    if len(subnode) == 1:
        tree.add_edge(root, subnode[0])
        return tree

    # 5-10: If connected component is two nodes
    if len(subnode) == 2:
        tree.add_edge(root, subnode[0])
        tree.add_edge(root, subnode[1])
        return tree

    # Insertion of root into tree, or adding edge between old and new root
    if root is None:
        tree.add_node(new_root)
    else:
        tree.add_edge(root, new_root)

    # 11: Dictionary, that counts how many nodes of the positions each tree has
    # -> semiuniversal nodes are alone in their tree
    dic = {}
    for node in position.nodes:
        e = node.tree
        if e not in dic:
            dic[e] = [node]
        elif e in dic:
            dic[e].append(node)

    # Determining semiuniversal nodes
    semiuniversal_nodes = [node for node in position.nodes if
                           len(dic[node.tree]) == 1 and not Tree.is_leaf(node.tree, node)]

    # Replacing semiuniversal nodes with their children
    for node_s in semiuniversal_nodes:
        for node_p in position.nodes:
            if node_s == node_p:
                position.nodes.remove(node_p)
                position.nodes.extend(nx.DiGraph.successors(node_p.tree, node_s))

    # Cluster Instersection Graph
    graph = cluster_intersection_graph(position.nodes)

    # Connected components
    components = list(nx.algorithms.components.connected_components(graph))

    # 15: Min cut if only one connected component
    if len(components) < 2:
        undirected = undirect(graph)
        cut_value, partition = nx.stoer_wagner(undirected, weight='bitscore')
        components = partition
        print('Only one connected component identified. Min cut done with stoer wagner. Cut value: %s' % cut_value)

    # 16-21: Recursive calls on connected components
    for w in components:
        BUILDST(Position(list(w)), tree, new_root)

    # 22:
    return tree


def undirect(graph):
    outgraph = nx.Graph()
    for node in graph.nodes():
        outgraph.add_node(node)
    for edge in graph.edges():
        outgraph.add_edge(edge[0], edge[1])
    return outgraph


def cluster_intersection_graph(position):
    """
    :param position: Initially the roots of the profile trees.
    :return: cluster intersection graph to show connected components
    """
    cluster_int_graph = nx.Graph()

    for node in position:
        # Get all leaves that are successors to node in position
        node_leaves = node.tree.get_node_leaves(node)

        nodes = list(cluster_int_graph.nodes())
        # leaf_dic = nx.get_node_attributes(cluster_int_graph, 'leaves')
        for node_in_graph in nodes:
            intersection = len(set.intersection(node_leaves, node_in_graph.tree.get_node_leaves(node_in_graph)))
            if intersection > 0:
                cluster_int_graph.add_edge(node, node_in_graph, weight=intersection)  # weight?
        cluster_int_graph.add_node(node, leaves=node_leaves)

    # nx.draw_shell(cluster_int_graph, node_shape='.', arrows=False)
    # plt.show()
    return cluster_int_graph
