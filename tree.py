import itertools
import random
import networkx as nx
from numpy.random.mtrand import choice
import cbmg
import knoten
import triple as t
from knoten import Knoten
import matplotlib.pyplot as plt
import sys


class Tree(nx.DiGraph):
    def __init__(self):
        super().__init__()

    def __new__(cls, tr=None):
        """
        With this, the DiGraph-__new__ function is overwritten, so that only objects of subclasses Tree or CBMG will
        be created when the DiGraph constructor is called
        """
        if cls == nx.DiGraph:
            if tr is None:
                return object.__new__(Tree)
            return object.__new__(cbmg.CBMG)
        return object.__new__(cls)

    nx.DiGraph.__new__ = staticmethod(__new__)

    @staticmethod
    def add_random_child(parent_list, tree):
        """
        Used in random_tree function
        :param parent_list: List of valid parents for the child
        :param tree: The existing tree, where the child will be inserted
        """
        parent = parent_list[random.randint(0, len(parent_list) - 1)]
        childname = parent.name + str(len(tree.nodes()))
        child = Knoten(childname)
        tree.add_edge(parent, child)

    @staticmethod
    def random_tree(max_nodes, depth, max_branches=2, min_branches=2):
        """
        Iteratively creates a random tree.
        :param max_nodes: Maximum nodes the finished tree is supposed to have
        :param depth: Maximum depth the finished tree is supposed to have
        :param max_branches: Maximum children an inner node is allowed to have
        :param min_branches: Minimum children an inner node must have
        :return: A random tree with the above parameters
        """
        tree = Tree()

        root = Knoten('r')
        tree.add_node(root)

        # Loop over max_nodes, each iteration inserts one node
        for node_ctr in range(max_nodes - 1):
            # Check for parents that have only one or no child, to which children will be added first
            sparse_parents = [node for node in tree.nodes() if len(list(tree.successors(node))) < min_branches
                              and not tree.is_leaf(node)]
            if sparse_parents:
                # Add a child to one of parent from sparse_parents, chosen at random
                Tree.add_random_child(sparse_parents, tree)
            else:
                # Also take leaves as parents if they are not at max depth. If at max depth, the tree is returned.
                parents = [node for node in tree.nodes() if len(list(tree.successors(node))) < max_branches
                           and tree.get_height(node) < depth]
                # If there are not enough unset nodes left to fulfill the min_branches condition for new parent:
                if node_ctr >= max_nodes - min_branches:
                    parents = [node for node in tree.nodes() if len(list(tree.successors(node))) < max_branches
                               and not tree.is_leaf(node)]
                if not parents:
                    return tree
                Tree.add_random_child(parents, tree)
        return tree

    @staticmethod
    def newick_to_tree(newick, tree=None, root=None):
        """
        :param newick: String in Newick format
        :param tree:
        :param root:
        :return: Object of class Tree
        """
        if tree is None:
            tree = Tree()

        newick_stripped = newick[1:len(newick) - 1]
        subtrees = Tree.get_newick_subtrees(newick_stripped)

        if root is None:
            name = ''.join(subtrees)
            root = Knoten(name)
            tree.add_node(root)

        # Adding the children and recursive calls on the subtrees
        for sub in subtrees:
            child = Knoten(sub)
            tree.add_edge(root, child)
            Tree.newick_to_tree(sub, tree, child)
        return tree

    @staticmethod
    def get_newick_subtrees(string):
        """
        Used in newick_to_tree
        :param string: A 'naked' tree in Newick format (without the most outer brackets)
        :return: A list of subtrees in Newick format
        """
        subtrees = []
        first = 0
        counter = 0
        # Uses counter to keep track of opening and closing brackets. First signifies the first character of
        # the current subtree
        for i in range(len(string)):
            if string[i] == '(':
                if counter == 0:
                    first = i
                counter += 1
            elif string[i] == ')':
                counter -= 1

            # If the counter reaches zero, a subtree is identified.
            if counter == 0 and string[i] == ',':
                # I don't exactly know why the commas need to be counted here, but it is needed, if the leafs have
                # names with more than one character
                if string.count(',') > 0:
                    subtrees.append(string[first: i])
                first = i + 1
            elif counter == 0 and i == len(string) - 1:
                if string.count(',') > 0:
                    subtrees.append(string[first: i + 1])
        return subtrees

    def __str__(self):
        return self.tree_to_newick()

    def is_leaf(self, node):
        """
        :param node: Node in tree to be checked
        :return: True if the node has no successors (i.e. is a leaf), False otherwise
        """
        if len(list(nx.DiGraph.successors(self, node))) == 0:
            return True
        return False

    def get_node_leaves(self, node):
        """
        :param node: Node in tree to get leaves from
        :return: The set of all leaves that node is a parent to
        """
        return set([x for x in nx.algorithms.traversal.dfs_preorder_nodes(self, node) if self.is_leaf(x)])

    def lca(self, one, two):
        """
        :param one: First node
        :param two: Second node
        :return: The last common ancestor node of the two nodes
        """
        parents_one = set(nx.shortest_path(self, self.get_root(), one))
        parents_two = set(nx.shortest_path(self, self.get_root(), two))
        schnittmenge = set.intersection(parents_one, parents_two)

        last_common_ancestor = self.get_root()
        for ancestor in schnittmenge:
            if self.get_height(ancestor) > self.get_height(last_common_ancestor):
                last_common_ancestor = ancestor
        return last_common_ancestor

    def get_height(self, node):
        """
        :param node: Node/ leaf
        :return: Height of node (Root has height 1)
        """
        return len(list(nx.shortest_path(self, self.get_root(), node)))

    def get_leaveset_from_tree(self):
        """
        :return: List of all leaves of the tree
        """
        leaf_nodes = []
        for node in self.nodes():
            if self.is_leaf(node):
                leaf_nodes.append(node)
        return leaf_nodes

    def find_best_match(self, node, nodes):
        """
        Compares node with nodes and find the node out of nodes that has the lowest lca with node (the best match).
        All nodes in nodes should have the same colour and node should be of a different colour.
        :param node:
        :param nodes: list of nodes from which the best match to 'node' is to be determined
        :return: Best match to node from nodes
        """
        requested = [nodes[0]]
        for potential_node in nodes:
            lca_pot = self.lca(node, potential_node)
            lca_req = self.lca(node, requested[0])
            if self.get_height(lca_pot) > self.get_height(lca_req):
                requested = [potential_node]
            elif self.get_height(lca_pot) == self.get_height(lca_req):
                requested.append(potential_node)
        return requested

    def paint_tree(self, colours):
        """
        Colours the leaves of a tree randomly with the colours from colours
        :param colours: List of colours
        :return: A coloured tree
        """
        for node in self.get_leaveset_from_tree():
            new_leaf = knoten.Knoten(node.name, choice(colours))
            if nx.DiGraph.predecessors(self, node):
                parent = list(nx.DiGraph.predecessors(self, node))[0]
                self.remove_node(node)
                self.add_edge(parent, new_leaf)
        return self

    def get_triples_recursively(self, tripels=[]):
        """
        Recursively generates triples from the leavesets of subtrees
        :param tripels:
        :return: List of tripels
        """
        root = self.get_root()
        children = set(nx.DiGraph.successors(self, root))
        for child_in in children:
            subt = nx.dfs_tree(self, child_in)
            # If there are two subtrees, this loop will only do one iteration
            for child_out in set.difference(children, {child_in}):
                ingroup = subt.get_leaveset_from_tree()
                outgroup = nx.dfs_tree(self, child_out).get_leaveset_from_tree()
                '''
                The triples can be generated from the leavesets of the two (or more) subtrees, but there will be
                duplicates. The function get_triples_from_leavesets deals with this directly. 
                '''
                tripels_to_add = self.get_triples_from_leavesets(ingroup, outgroup)
                tripels.extend(tripels_to_add)

            subt.get_triples_recursively(tripels)
        return tripels

    @staticmethod
    def get_triples_from_leavesets(ingroup, outgroup):
        """
        :param ingroup: Leaveset from first subtree
        :param outgroup: Leaveset from second subtree
        :return: All possible triples that can be generated
        """
        tripels = []
        for x in ingroup:
            # x and y cannot be the same
            for y in set.difference(set(ingroup), {x}):
                for z in outgroup:
                    # If x < y, triple (x,y|z) will be appended, the identical triple (y,x|z) will not
                    if x < y:
                        tripel = t.Triple(x, y, z)
                        tripels.append(tripel)
        return tripels

    @staticmethod
    def get_leaveset_from_triples(triples):
        """
        :param triples: List of triples
        :return: List of leaves that can be found in the triples
        """
        leaves = []
        for tr in triples:
            if tr.x not in leaves:
                leaves.append(tr.x)
            if tr.y not in leaves:
                leaves.append(tr.y)
            if tr.z not in leaves:
                leaves.append(tr.z)
        return leaves

    def tree_to_newick(self):
        # to_newick can't do outermost brackets
        return '(' + Tree.to_newick(self) + ')'

    def to_newick(self, root=None):
        """
        :param root: None at first, is used internally to add children
        :return: String of the tree in Newick format without the outermost brackets
        """
        teile = []
        tree_copy = nx.DiGraph.copy(self).to_directed()

        if root is None:
            root = self.get_root()

        if self.is_leaf(root):
            try:
                teile.append(root.name + '.' + root.colour)
            except TypeError:
                teile.append(root.name)

        elif len(list(nx.DiGraph.successors(tree_copy, root))) > 0:
            for node in nx.DiGraph.successors(tree_copy, root):
                subtree = nx.dfs_tree(tree_copy, node)
                subt = Tree.to_newick(subtree, node)
                if self.is_leaf(node):
                    teile.append(subt)
                else:
                    teile.append('(' + subt + ')')

        return ','.join(teile)

    def get_root(self):
        """
        :return: Node that has no predecessors (root)
        """
        if not nx.is_weakly_connected(self):
            print("graph is at least a forest , but not a single tree , there is no single root");
            nx.draw_shell(self, node_shape='.', with_labels=True)
            plt.show()
            sys.exit(1)
        for node in self.nodes():
            if len(list(nx.DiGraph.predecessors(self, node))) == 0:
                return node

        print(" graph is circular, no root found")
        sys.exit(1)

    @staticmethod
    def construct_hasse(hierarchy):
        """
        Constructs tree from hasse hierarchy
        :param hierarchy: List of sets or lists
        :return: Tree representing the hierarchy
        """
        tree = Tree()

        '''
        For every permutation of two subsets it is checked whether one is a subset of the other. If so,
        an edge is made
        '''
        for permutation in itertools.permutations(hierarchy, 2):
            first = permutation[0]
            second = permutation[1]
            if set(first).issubset(set(second)) and first != second:
                if len(second) > 1:
                    sec = Knoten(second[0].name + "_" + str(len(second)), node_set=set(second), tree=tree)
                else:
                    sec = Knoten(second[0].name, node_set=set(second), tree=tree)

                if len(second) == 1:
                    sec.colour = second[0].colour

                if len(first) > 1:
                    fst = Knoten(first[0].name + "_" + str(len(first)), node_set=set(first), tree=tree)
                else:
                    fst = Knoten(first[0].name, node_set=set(first), tree=tree)
                if len(first) == 1:
                    fst.colour = first[0].colour

                tree.add_edge(sec, fst)

        '''
        Unnecessary edges are removed: A leave is has egdes to all sets of which it is a subsset. The smallest of those
        sets is the direct parent, edges to every other set are removed
        '''
        for node in tree.nodes():
            parents = list(tree.predecessors(node))
            parents.sort(key=len)

            for parent in parents[1:]:
                tree.remove_edge(parent, node)

        if not nx.is_weakly_connected(tree):
            print("Hasse diagram has more than one component")
            sys.exit(1)

        return tree

    @staticmethod
    def make_triples_inconsistent(tripelset, percentage):
        """
        :param tripelset:
        :param percentage: The percentage of triples to be made inconsistent
        :return: A tripleset with an amount of inconsistent triples
        """
        random.shuffle(tripelset)

        tripels = []
        tripels.extend(tripelset)

        to_make_inconsistent = tripelset[: int(percentage * len(tripelset))]

        for tripel in to_make_inconsistent:
            # Inconsistent triples are made by swapping the outgroup with one member of the ingroup
            inconsistent = t.Triple(tripel.z, tripel.y, tripel.x, bitscore=tripel.bitscore / 10, evalue=tripel.evalue)
            tripels.append(inconsistent)

        return tripels

    @staticmethod
    def remove_triple(tripleset, percentage):
        """
        :param tripleset: 
        :param percentage: Percentage of triples to be removed
        :return: A smaller tripleset
        """
        random.shuffle(tripleset)

        shorter_tripleset = tripleset[int(percentage * len(tripleset)):]

        return shorter_tripleset
    @staticmethod
    def subtree (tree, nodes ):
        """
        basically the subgraph method for directed grahs...

        :param tree: input tree
        :param nodes: nodes that iput tree should be constrained to
        :return: subtree containing all input nodes and the corresponding edges
        """
        outree = Tree()
        for node in tree:
            outree.add_node(node)
        for edge in tree:
            if edge[0] in outree.nodes and edge[1] in outree.nodes:
                outree.add_edge(edge)


