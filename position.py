import networkx as nx


class Position:
    def __init__(self, nodes):
        self.nodes = nodes

    def subnodes(self):
        sub_nodes = []
        for node in self.nodes:
            # append all successors of node
            sub_nodes.extend(nx.algorithms.dfs_preorder_nodes(node.tree, node))
        return sub_nodes
