class Knoten:
    def __init__(self, name, colour=None, thinness=None, in_neighbours=None, out_neighbours=None, tree=None,
                 node_set=None):
        """
        :param name: Unique name of the node
        :param colour: Colour of the node (optional)
        :param thinness: Thinness class of the node. Is assigned by the mark_thinness() function from the CBMG class
        :param in_neighbours: In CBMG: In-neighbours of the node
        :param out_neighbours: In CBMG: Out-neighbours of the node
        :param tree: Tree, the node is associated with. Is assigned in Profile generation
        :param node_set: The leaf-set that are children to this node in a hasse diagram
        """
        self.name = name
        self.colour = colour
        self.thinness = thinness
        self.in_neighbours = in_neighbours
        self.out_neighbours = out_neighbours
        self.tree = tree
        self.node_set = node_set

    def __str__(self):
        return self.name

    def __len__(self):
        return len(self.node_set)

    def __lt__(self, other):
        return self.name < other.name

    def __gt__(self, other):
        return self.name > other.name

    def __le__(self, other):
        return self.name <= other.name

    def __ge__(self, other):
        return self.name >= other.name

    def __eq__(self, other):
        return self.name == other.name

    def __ne__(self, other):
        return self.name != other.name

    def __hash__(self):
        return hash(self.name)

    def get_all_info(self):
        return 'Id: %s, Col: %s' % (str(self.name), str(self.colour))
