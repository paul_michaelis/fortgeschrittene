import csv
import networkx as nx
import knoten
from cbmg import CBMG


def parse_protein_ortho():
    bmg = nx.DiGraph()
    bmg.__class__ = CBMG
    with open("test.blast-graph", "r") as infile:
        csvreader = csv.reader(infile, delimiter="\t")
        tsvlist = list(csvreader)[2:]

        filename_a = ''
        filename_b = ''

        for row in tsvlist:
            if "Score" in row[0]:
                continue
            elif "#" in row[0]:
                filename_a = row[0][2:]
                filename_b = row[1]
            else:
                node_a = knoten.Knoten(filename_a + "_" + row[0], colour=row[0][0])
                node_b = knoten.Knoten(filename_b + "_" + row[1], colour=row[1][0])
                if node_a.name not in bmg:
                    bmg.add_node(node_a.name, name = node_a.name, colour=node_a.colour)
                if node_b.name not in bmg:
                    bmg.add_node(node_b.name, name = node_b.name, colour=node_b.colour)
                if len(row) > 7:
                    # not both files have a similarity score...
                    bmg.add_edge(node_a.name, node_b.name, evalue=float(row[2]), bitscore=int(row[3]),
                                 simscore=float(row[7]))
                    bmg.add_edge(node_b.name, node_a.name, evalue=float(row[4]), bitscore=int(row[5]),
                                 simscore=float(row[7]))
                else:
                    bmg.add_edge(node_a.name, node_b.name, evalue=float(row[2]), bitscore=int(row[3]))
                    bmg.add_edge(node_b.name, node_a.name, evalue=float(row[4]), bitscore=int(row[5]))

    leaf_dict = {}
    for node in bmg.nodes():
        leaf_dict.setdefault(node.colour, []).append(node)

    cols = set(leaf_dict.keys())
    bmg.colours = cols

    CBMG.mark_thinness(bmg)

    return bmg
