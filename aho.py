import networkx as nx
import knoten
from tree import Tree


def aho(tripleset, leafset=None, tree=None, root=None):
    """
    Aho BUILD algorithm that creates a Tree object from a list of triples
    :param tripleset: list of triples
    :param leafset: None at initialisation, is extrected from the triples
    :param tree: None at initialisation, is created in the beginning of the algorithm
    :param root: None at initialisation, is created in the beginning of the algorithm
    :return: A Tree object
    """
    if tree is None:
        tree = nx.DiGraph()

    if leafset is None:
        leafset = Tree.get_leaveset_from_triples(tripleset)
        
    # if only one leaf return it as tree    
    if len(leafset) == 1:
        tree.add_edge(root, leafset[0])
        return tree

    # Konstruiere Hilfsgraph A
    hilfsgraph = construct_a_nx(tripleset, leafset)

    # Identifiziere Zusammenhangskomponenten in A
    components = list(nx.algorithms.components.connected_components(hilfsgraph))

    '''
    Stoer Wagner algorithm is used to do a min cut if there is only one connected component. If the triples have 
    bitscores as information, this information is used to weigh the cuts.
    '''
    if len(components) == 1:
        cut_value, partition = nx.stoer_wagner(hilfsgraph, weight='bitscore')
        # new list of components after cut
        components = partition
        # print('Only one connected component identified. Min cut done with stoer wagner. Cut value: %s' % cut_value)

    # Create unique node names for inner nodes
    node_name = ''
    for node in leafset:
        node_name += node.name

    # Create root at beginning of algorithm
    if root is None:
        new_root = knoten.Knoten(node_name)
        tree.add_node(new_root)
    else:
        new_root = knoten.Knoten(node_name)
        tree.add_edge(root, new_root)

    # Recursive calls of algorithm on connected components
    for component in components:
        new_r = []
        new_l = list(component)

        for tripel in tripleset:
            if tripel.x in new_l and tripel.y in new_l and tripel.z in new_l:
                new_r.append(tripel)

        aho(new_r, new_l, tree, new_root)

    return tree


def construct_a_nx(r, l):
    """
    Contructs intermediate graph showing connected. Bitscores and evalues are integrated into the graph
    :param r: List of Triple objects
    :param l: List of leaf nodes
    :return: Undirected networkx graph
    """
    graph = nx.Graph()
    for i in l:
        for tripel in r:
            if i == tripel.x:
                graph.add_edge(tripel.x, tripel.y, bitscore=tripel.bitscore, evalue=tripel.evalue)
            elif i == tripel.y:
                graph.add_edge(tripel.x, tripel.y, bitscore=tripel.bitscore, evalue=tripel.evalue)
        graph.add_node(i)
    return graph
