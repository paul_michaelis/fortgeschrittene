import numpy as np
import matplotlib.pyplot as plt
#import plotly.plotly as py
#import plotly.tools as tls
import math
import numpy as np
from mpl_toolkits.mplot3d import Axes3D

"""
compare runtime  of Build to BuildST 
under the effect of number of colours for 50/100/200 verges

"""
#        50        100    150 verges
aho3c =  [0.0125, 0.0422, 0.1444]
aho4c =  [0.0321, 0.2604, 2.5125]
aho10c = [0.0203, 0.3089, 4.6737]
aho20c = [0.0185, 0.1793, 3.4063]

#             4     10     20 colours
aho_50v = [0.0321,0.0203,0.0185]
aho_100v = [0.2604,0.3089,0.179]
aho_200v = [2.5125, 4.6737, 3.4063]

deng3c = [0 , 0, 0] # this data was not collected
deng4c =  [0.0297, 0.0464, 0.1827]
deng10c = [0.0865, 0.4408, 1.5896]
deng20c = [0.1133, 0.7418, 4.9422]

deng_50v = [0.0297,0.0865,0.1133]
deng_100v =[0.0464,0.4408,0.7418]
deng_200v = [0.0203, 1.5896, 4.9422]


barWidth = 0.2 
# x position on bar
r1 = np.arange(len(aho_50v))
r2 = [x + barWidth for x in r1]
r3 = [x - barWidth for x in r1]

plt.bar(r1, aho_50v, width = barWidth, color = 'blue', edgecolor = 'black',  label='4 colours')
plt.bar(r2, aho_100v, width = barWidth, color = 'cyan', edgecolor = 'black', label='10 colours')
plt.bar(r3, aho_200v, width = barWidth, color = 'red', edgecolor = 'black', label='20 colours')

#plt.bar(r1, deng_50v, width = barWidth, color = 'blue', edgecolor = 'black',  label='4 colours')
#plt.bar(r2, deng_100v, width = barWidth, color = 'cyan', edgecolor = 'black', label='10 colours')
#plt.bar(r3, deng_200v, width = barWidth, color = 'red', edgecolor = 'black', label='20 colours')

plt.xticks([r + barWidth for r in range(len(aho4c))], ['4 colours', '10 colours', '20 colours'])
plt.ylabel('time (s)')
plt.xlabel('Number of verges in the random tree')
plt.title("Runtime comparison different number of colours")
plt.legend()
plt.show()
#plt.savefig("aho_deng_runtime_copmarison_10c.png")
