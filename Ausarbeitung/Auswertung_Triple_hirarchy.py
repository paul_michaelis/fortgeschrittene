import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc

countries = ['3', '5', '10', '20']
triple = np.array([0.0761, 0.0716 , 0.0429, 0.02619])
aho = np.array([0.0525, 0.0521, 0.0332, 0.0184])
hasse = np.array([0.01555, 0.0258 , 0.0403, 0.0561])
deng = np.array([0.0144, 0.0500, 0.1406, 0.2316])
#golds = np.array([46, 27, 26, 19, 17])
ind = [x for x, _ in enumerate(countries)]
barWidth = 0.2 
# x position on bar
r1 = np.arange(len(triple))
r2 = [x + barWidth for x in r1]
#plt.bar(ind, golds, width=0.8, label='golds', color='gold', bottom=silvers+bronzes)
plt.bar(r1, aho, width = barWidth, label='BUILD', color='#00BFFF', bottom=triple)
plt.bar(r1, triple, width = barWidth, label='generate triple', color='blue')
plt.bar(r2, deng, width = barWidth, label='BUILD_ST', color='#00FF00', bottom=hasse)
plt.bar(r2, hasse, width = barWidth, label='generate profile', color='green')

plt.xticks(ind, countries)
plt.ylabel("runtime (s)")
plt.xlabel("amount of colours")
plt.legend(loc="")
#plt.title("50 verges")

plt.show()
"""
countries = ['3', '5', '10', '20']
triple100 = np.array([0.6723, 0.4228 , 0.2459, 0.1974])
aho100 = np.array([0.6453, 0.4770, 0.3654, 0.2584])
hasse100 = np.array([0.0675, 0.0813 , 0.1168, 0.1816])
deng100 = np.array([0.03812, 0.1158, 0.4942, 1.1647])
#golds = np.array([46, 27, 26, 19, 17])
ind = [x for x, _ in enumerate(countries)]
barWidth = 0.2 
# x position on bar
r1 = np.arange(len(triple100))
r2 = [x + barWidth for x in r1]
#plt.bar(ind, golds, width=0.8, label='golds', color='gold', bottom=silvers+bronzes)
plt.bar(r1, aho100, width = barWidth, label='generate aho tree', color='#00BFFF', bottom=triple100)
plt.bar(r1, triple100, width = barWidth, label='generate triple', color='blue')
plt.bar(r2, deng100, width = barWidth, label='generate deng tree', color='#00FF00', bottom=hasse100)
plt.bar(r2, hasse100, width = barWidth, label='generate hasse', color='green')

plt.xticks(ind, countries)
plt.ylabel("runtime (s)")
plt.xlabel("amount of colours")
plt.legend(loc="")
plt.title("")

plt.show()

countries = ['3', '5', '10', '20']
triple200 = np.array([4.2955, 3.1840 , 2.1239, 1.276])
aho200 = np.array([4.2210, 5.6252, 4.1902, 3.5342])
hasse200 = np.array([0.1998, 0.2649 , 0.3824, 0.4759])
deng200 = np.array([0.08956, 0.3273, 1.5981, 5.7612])
#golds = np.array([46, 27, 26, 19, 17])
ind = [x for x, _ in enumerate(countries)]
barWidth = 0.2 
# x position on bar
r1 = np.arange(len(triple200))
r2 = [x + barWidth for x in r1]
#plt.bar(ind, golds, width=0.8, label='golds', color='gold', bottom=silvers+bronzes)
plt.bar(r1, aho200, width = barWidth, label='BUILD', color='#00BFFF', bottom=triple200)
plt.bar(r1, triple200, width = barWidth, label='generate triple', color='blue')
plt.bar(r2, deng200, width = barWidth, label='BUILD_ST', color='#00FF00', bottom=hasse200)
plt.bar(r2, hasse200, width = barWidth, label='generate profile', color='green')

plt.xticks(ind, countries)
plt.ylabel("runtime (s)")
plt.xlabel("amount of colours")
plt.legend(loc="")
plt.title("")

plt.show()

"""